import {remote} from 'electron';
import React from 'react';
import Slider from 'react-slick';
import retry from 'async-retry';
import toMilliseconds from '@sindresorhus/to-milliseconds';
import {global} from '../styles/app';

class App extends React.PureComponent {
  state = {
    contents: [],
    hasLoaded: false
  }

  async updateContent() {
    const contents = await retry(async bail => {
      const res = await fetch(
        'https://rpi-kiosk.s3.amazonaws.com/divya/content.json'
      );

      if (res.status === 403) {
        bail(new Error('Unauthorized'));
        return;
      }

      const data = await res.json();

      return data;
    }, {
      retries: 5
    });

    this.setState({
      contents,
      hasLoaded: true
    });
  }

  checkForContent() {
    setInterval(() => {
      this.updateContent();
    }, toMilliseconds({hours: 4}));

    this.updateContent();
  }

  onKeyDown = event => {
    if (event.key === 'Escape') {
      if (remote.getCurrentWindow().isFullScreen()) {
        remote.getCurrentWindow().setFullScreen(false);
      }
    }
  }

  componentDidMount() {
    this.checkForContent();

    document.addEventListener('keydown', this.onKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.onKeyDown);
  }

  render() {
    const settings = {
      adaptiveHeight: true,
      arrows: false,
      infinite: true,
      draggable: false,
      accessibility: false,
      autoplay: true,
      autoplaySpeed: 5000,
      speed: 1000,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnFocus: false,
      pauseOnHover: false
    };

    if (!this.state.hasLoaded) {
      return null;
    }

    return (
      <main>
        <Slider {...settings}>
          {this.state.contents.map(content => (
            <section key={content}>
              <img src={content}/>
            </section>
          ))}
        </Slider>

        <style jsx global>
          {global}
        </style>
      </main>
    );
  }
}

export default App;
