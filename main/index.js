'use strict';

const {app, BrowserWindow} = require('electron');
const prepareNext = require('electron-next');
const isDev = require('electron-is-dev');
const log = require('electron-log');
const {autoUpdater} = require('electron-updater');
const toMilliseconds = require('@sindresorhus/to-milliseconds');

if (!isDev) {
  app.setLoginItemSettings({
    openAtLogin: true
  });
}

const checkForUpdates = () => {
  if (isDev) {
    return false;
  }

  // For auto-update debugging in Console.app
  autoUpdater.logger = log;
  autoUpdater.logger.transports.file.level = 'info';

  setInterval(() => {
    autoUpdater.checkForUpdates();
  }, toMilliseconds({hours: 1}));

  autoUpdater.checkForUpdates();
};

const loadRoute = (win, routeName) => {
  if (isDev) {
    win.loadURL(`http://localhost:8000/${routeName}`);
    win.openDevTools({mode: 'detach'});
  } else {
    win.loadFile(`${app.getAppPath()}/renderer/out/${routeName}/index.html`);
  }
};

const openWindow = () => {
  const editorWindow = new BrowserWindow({
    fullscreen: !isDev,
    webPreferences: {
      webSecurity: !isDev // Disable webSecurity in dev to load video over file:// protocol while serving over insecure http, this is not needed in production where we use file:// protocol for html serving.
    },
    show: false
  });

  loadRoute(editorWindow, 'app');

  editorWindow.webContents.on('did-finish-load', () => {
    editorWindow.show();
  });
};

// Prepare the renderer once the app is ready
(async () => {
  await app.whenReady();

  await prepareNext('./renderer');

  openWindow();

  checkForUpdates();
})();

app.on('window-all-closed', () => {
  app.quit();
});
